#include <cstdint>

#include <CUnit/CUnit.h>
#include <Decoder.h>
#include <log.h>


/**
 * Test that when writing more data than the decoder's buffer can hold, no
 * buffer overflow occurs.
 */
void test_write_overflow()
{
    uint8_t buf[uthernet::MIN_FRAME_SIZE];
    uint8_t frame[] = {
        0xca, 0xfe, 0x00, 0xa, 0x0,
        0xaa, 0xaa, 0xbb, 0xbb, 0x0,
        0x00, 0x00, 0x11, 0x11,
        0x0, 0x0,
    };
    size_t ret;

    uthernet::Decoder d(buf, sizeof(buf), nullptr, nullptr);

    ret = d.feed(frame, sizeof(frame));
    CU_ASSERT_EQUAL(ret, uthernet::MIN_FRAME_SIZE);
}


void test_free_space()
{
    uint8_t buf[] = {
        0xca, 0xfe, 0x0, 0x7, 0x0,
        0xff, 0xff, 0xaa, 0xaa, 0x0,
        0x0, 0x0
    }; /* A valid, empty frame */
    unsigned long i;

    uthernet::Decoder d(buf, sizeof(buf), nullptr, nullptr);

    CU_ASSERT_EQUAL(d.free_space(), sizeof(buf));
    for (i = 0; i < sizeof(buf); i++) {
        d.reset();
        d.feed(buf, i);
        print_debug("free space: %zu\n\n", d.free_space());
        CU_ASSERT_EQUAL(d.free_space(), sizeof(buf) - i);
        CU_ASSERT_PTR_EQUAL(d.write_head(), d.buffer() + i);
    }
}

CU_TestInfo general_tests[] = {
        {"free space", test_free_space},
        {"write overflow", test_write_overflow},
        CU_TEST_INFO_NULL
};
