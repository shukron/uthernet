#include <arpa/inet.h>

#include <CUnit/CUnit.h>
#include <Decoder.h>

#include "log.h"

using namespace uthernet;

static enum Decoder::Error s_error;

static void on_error(Decoder *decoder, Decoder::Error error)
{
	print_debug("on error called with error: %d\n", error);
	s_error = error;
}

static void sanity()
{
	uint8_t buf[MIN_FRAME_SIZE];
	uint8_t data[] = {0xca, 0xfe, 0x0, 0x7, 0x0};

	Decoder d(buf, MIN_FRAME_SIZE, nullptr, nullptr);
	d.feed(data, sizeof(data));
	CU_ASSERT_EQUAL(d.current_state(), Decoder::CONTENT_CRC);
	CU_ASSERT_EQUAL(d.read_next(), 7);
}

static void test_invalid_size(uint16_t size)
{
	
	uint8_t buf[MIN_FRAME_SIZE];
	const uint8_t hec = 0;

	s_error = Decoder::NO_ERROR;
	size = htons(size);
	Decoder d(buf, MIN_FRAME_SIZE, nullptr, on_error);
	d.feed(barker, sizeof(barker));
	d.feed((uint8_t *)&size, sizeof(size));
	d.feed((uint8_t *)&hec, sizeof(hec));
	CU_ASSERT_EQUAL(d.current_state(), Decoder::BARKER);
	CU_ASSERT_EQUAL(s_error, Decoder::INVALID_SIZE);
	s_error = Decoder::NO_ERROR;
}

static void size_too_big()
{
	test_invalid_size(MIN_FRAME_SIZE - FRAME_HEADER_SIZE + 1);
}

static void size_too_small()
{
	test_invalid_size(MIN_FRAME_SIZE - FRAME_HEADER_SIZE - 1);
}

static void corrupted_size_unfixable()
{
	
	uint8_t buf[MIN_FRAME_SIZE];
	const uint8_t hec = 1;
	const uint16_t size = htons(MIN_FRAME_SIZE - FRAME_HEADER_SIZE);

	s_error = Decoder::NO_ERROR;
	Decoder d(buf, MIN_FRAME_SIZE, nullptr, on_error);
	d.feed(barker, sizeof(barker));
	d.feed((uint8_t *)&size, sizeof(size));
	d.feed((uint8_t *)&hec, sizeof(hec));
	CU_ASSERT_EQUAL(d.current_state(), Decoder::BARKER);
	CU_ASSERT_EQUAL(s_error, Decoder::HEC_ERROR);
	s_error = Decoder::NO_ERROR;
}

CU_TestInfo size_tests[] = {
	{"Sanity", sanity},
	{"Size too big", size_too_big},
	{"Size too small", size_too_small},
	{"Corrupted size unfixable", corrupted_size_unfixable},
	/* {"Corrupted size fixable", corrupted_size_fixable}, */
	CU_TEST_INFO_NULL
};
