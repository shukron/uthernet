#include <Decoder.h>

#include <cstdio>

#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>

#include "log.h"

extern CU_TestInfo barker_tests[];
extern CU_TestInfo general_tests[];
extern CU_TestInfo frame_tests[];
extern CU_TestInfo size_tests[];

static CU_SuiteInfo suites[] = {
        {"General", nullptr, nullptr, nullptr, nullptr, general_tests},
        {"Barker", nullptr, nullptr, nullptr, nullptr, barker_tests},
        {"Size", nullptr, nullptr, nullptr, nullptr, size_tests},
        {"Frame", nullptr, nullptr, nullptr, nullptr, frame_tests},
        CU_SUITE_INFO_NULL,
};

int main()
{
    unsigned int failed = 0;
    print_debug("%s\n", "Starting");

    /* initialize the CUnit test registry */
    if (CUE_SUCCESS != CU_initialize_registry())
        goto out;

    if (CUE_SUCCESS != CU_register_suites(suites))
        goto err_cleanup;

    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();
    failed = CU_get_number_of_tests_failed();

err_cleanup:
    CU_cleanup_registry();
out:
    return CU_get_error() || failed;
}