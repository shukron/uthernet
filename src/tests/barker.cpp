#include <CUnit/CUnit.h>
#include <log.h>
#include <cstdint>
#include <Decoder.h>

using namespace uthernet;

#define ARRAY_SIZE(array) \
    (sizeof(array) / sizeof((array)[0]))

static enum Decoder::Error s_error;

static void on_error(Decoder *decoder, enum Decoder::Error error)
{
    print_debug("on error called with error: %d\n", error);
    s_error = error;
}

static inline void assert_barker_found(Decoder *decoder)
{
    uint8_t barker[] = {0xca, 0xfe};
    CU_ASSERT_EQUAL(decoder->current_state(), Decoder::SIZE);
    CU_ASSERT_EQUAL(decoder->read_next(), 3);
    CU_ASSERT_NSTRING_EQUAL(
        decoder->buffer(),
        barker, sizeof(barker));
}

void test_barker_valid()
{
    uint8_t buf[MIN_FRAME_SIZE];
    unsigned int i;

    uint8_t case_a[] = {0xca, 0xfe};
    uint8_t case_b[] = {0xca, 0xca, 0xfe};
    uint8_t case_c[] = {0xfe, 0xca, 0xca, 0xca, 0xfe};
    uint8_t *test_data[] = {case_a, case_b, case_c};
    /* Ugh, figure out a better way to do this */
    size_t sizes[] = {sizeof case_a, sizeof case_b, sizeof case_c};

    Decoder d(buf, sizeof(buf), nullptr, nullptr);

    for (i = 0; i < ARRAY_SIZE(test_data); i++) {
        d.reset();
        d.feed(test_data[i], sizes[i]);
        assert_barker_found(&d);
    }
}

void test_no_barker()
{
    uint8_t buf[MIN_FRAME_SIZE];
    uint8_t no_start[] = {0xfe, 0xfe, 0xfe, 0xfe, 0xfe};
    unsigned long i;

    Decoder d(buf, sizeof(d), nullptr, nullptr);

    for (i = 0; i < sizeof(no_start); i++) {
        d.reset();
        d.feed(no_start, i);
        CU_ASSERT_EQUAL(d.current_state(), Decoder::BARKER);
    }
}

void test_barker_across_writes()
{
    uint8_t buf[MIN_FRAME_SIZE];
    uint8_t data[] = {0xfe, 0xfe, 0xfe, 0xca, 0xfe};
    uint8_t *last = &data[sizeof(data) - 1];

    Decoder d(buf, sizeof(buf), nullptr, nullptr);

    /* write all but the last byte */
    d.feed(data, sizeof(data) - 1);
    CU_ASSERT_EQUAL(d.current_state(), Decoder::BARKER);
    CU_ASSERT_EQUAL(d.read_next(), 1);
    d.feed(last, 1);
    assert_barker_found(&d);
}

void test_double_barker()
{
    uint8_t buf[MIN_FRAME_SIZE];
    uint8_t data[] = {0xca, 0xfe, 0xca, 0xfe, 0xaa};
    uint8_t *last = &data[sizeof(data) - 1];

    Decoder d(buf, sizeof(buf), nullptr, on_error);
    s_error = Decoder::NO_ERROR;
    d.feed(data, sizeof(data));
    CU_ASSERT_EQUAL(d.current_state(), Decoder::SIZE);
    CU_ASSERT_EQUAL(s_error, Decoder::HEC_ERROR);
    CU_ASSERT_EQUAL(d.read_next(), 2);
    CU_ASSERT_EQUAL(*d.read_head(), *last);
}

void test_barker_across_boundaries()
{
    uint8_t buf[MIN_FRAME_SIZE];
    uint8_t data[] = {
        0xfe, 0xfe, 0xfe, 0xfe,
        0xfe, 0xfe, 0xfe, 0xfe,
        0xfe, 0xfe, 0xfe, 0xca, 0xfe
    };
    uint8_t *last = &data[sizeof(data) - 1];
    size_t ret;

    Decoder d(buf, sizeof(buf), nullptr, nullptr);

    /* write all but the last byte */
    ret = d.feed(data, sizeof(data) - 1);
    CU_ASSERT_EQUAL(ret, MIN_FRAME_SIZE);
    CU_ASSERT_EQUAL(d.current_state(), Decoder::BARKER);
    CU_ASSERT_EQUAL(d.read_next(), 1);
    d.feed(last, 1);
    assert_barker_found(&d);
}

CU_TestInfo barker_tests[] = {
    {"No Barker", test_no_barker},
    {"Valid Barker", test_barker_valid},
    {"Barker Across Writes", test_barker_across_writes},
    {"Double Barker", test_double_barker},
    {"Barker Across Buffer Boundaries", test_barker_across_boundaries},
    CU_TEST_INFO_NULL
};
