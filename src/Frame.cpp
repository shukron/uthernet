#include "Frame.h"

using namespace uthernet;

Frame::Frame(uint8_t * fstart, size_t frame_size)
        : m_start{fstart}, m_frame_size{frame_size}, m_extension_bytes{0}
{
    init();
}

Frame::Frame(const Frame &frame)
        : Frame(frame.m_start, frame.m_frame_size) {}

Frame::Frame() : Frame(nullptr, 0) {}

void Frame::init()
{
    m_extension_bytes = 0;
    if (m_start == nullptr || m_frame_size < MIN_FRAME_SIZE)
        return;

    // Sets the read head on the flags field.
    uint8_t *head = m_start + FRAME_HEADER_SIZE + ROUTE_HEADER_SIZE - 1;

    while (EXTENSION_BIT(*head)) {
        m_extension_bytes++;
        head++;
    }
}

Frame &Frame::operator=(const Frame &frame) = default;
