FROM ubuntu:16.04

ENV PROJECT_HOME /code/uthernet
ENV BUILD_DIR ${PROJECT_HOME}/build

RUN apt-get update && apt-get -y upgrade
RUN apt-get install -y build-essential
RUN apt-get install -y libcunit1 libcunit1-doc libcunit1-dev
RUN apt-get install -y cmake

RUN mkdir -p ${BUILD_DIR}

COPY src ${PROJECT_HOME}/src
COPY CMakeLists.txt ${PROJECT_HOME}/
